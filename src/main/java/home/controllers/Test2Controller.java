package home.controllers;

import com.sun.org.apache.xerces.internal.util.FeatureState;
import home.entities.cassandra.Book2;
import home.reposiotries.Book2Repository;
import home.stateflow.events.FlowEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.statemachine.StateMachine;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class Test2Controller {

    private final Book2Repository book2Repository;
    private final StateMachine<FeatureState, FlowEvent> stateMachine;

    @Autowired
    public Test2Controller(Book2Repository book2Repository, StateMachine<FeatureState, FlowEvent> stateMachine) {
        this.book2Repository = book2Repository;
        this.stateMachine = stateMachine;
    }

    @RequestMapping("/test2/{title}/{autor}")
    public Book2 test2(@PathVariable String title, @PathVariable String autor){
        Book2 book2 = new Book2(title + autor, title, autor);
        book2Repository.save(book2);
        return book2;
    }

    @RequestMapping("/test22")
    public String test2(){
        return book2Repository.findAll().toString();
    }

    @RequestMapping("/testevent/{event}")
    public void test3(@PathVariable String event){
        Message<FlowEvent> message = MessageBuilder
                .withPayload(FlowEvent.valueOf(event))
                .setHeader("foo", "bar")
                .build();
        stateMachine.sendEvent(message);
    }

}
