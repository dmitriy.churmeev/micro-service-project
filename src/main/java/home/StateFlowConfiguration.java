package home;

import home.stateflow.events.FlowEvent;
import home.stateflow.states.FlowState;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.statemachine.config.EnableStateMachine;
import org.springframework.statemachine.config.EnumStateMachineConfigurerAdapter;
import org.springframework.statemachine.config.builders.StateMachineConfigurationConfigurer;
import org.springframework.statemachine.config.builders.StateMachineStateConfigurer;
import org.springframework.statemachine.config.builders.StateMachineTransitionConfigurer;
import org.springframework.statemachine.listener.StateMachineListener;
import org.springframework.statemachine.listener.StateMachineListenerAdapter;
import org.springframework.statemachine.state.State;

import java.util.EnumSet;

@Configuration
@EnableStateMachine
public class StateFlowConfiguration extends EnumStateMachineConfigurerAdapter<FlowState, FlowEvent> {
    @Override
    public void configure(StateMachineConfigurationConfigurer<FlowState, FlowEvent> config) throws Exception {
        config.withConfiguration().autoStartup(true).listener(listener());
    }

    @Override
    public void configure(StateMachineStateConfigurer<FlowState, FlowEvent> states) throws Exception {
        states.withStates().initial(FlowState.STATE).states(EnumSet.allOf(FlowState.class));
    }

    @Override
    public void configure(StateMachineTransitionConfigurer<FlowState, FlowEvent> transitions) throws Exception {
        transitions.withExternal().source(FlowState.STATE).target(FlowState.STATE1).event(FlowEvent.EVENT1)
                .and()
                .withExternal()
                .source(FlowState.STATE1).target(FlowState.STATE2).event(FlowEvent.EVENT2);
    }

    @Bean
    public StateMachineListener<FlowState, FlowEvent> listener() {
        return new StateMachineListenerAdapter<FlowState, FlowEvent>() {
            @Override
            public void stateChanged(State<FlowState, FlowEvent> from, State<FlowState, FlowEvent> to) {
                System.out.println("State change to " + to.getId());
            }
        };
    }
}
