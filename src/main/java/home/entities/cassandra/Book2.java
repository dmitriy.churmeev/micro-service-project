package home.entities.cassandra;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.cassandra.mapping.PrimaryKey;
import org.springframework.data.cassandra.mapping.Table;

import java.io.Serializable;


@Table("books2")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book2 implements Serializable{

    @PrimaryKey
    private String id;
    private String autor;
    private String title;
}
