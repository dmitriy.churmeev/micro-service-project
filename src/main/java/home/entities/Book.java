package home.entities;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigInteger;

@Data
@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue
    private BigInteger id;

    @Column(name = "name")
    private String name;

    @Column(name = "author")
    private String author;
}
