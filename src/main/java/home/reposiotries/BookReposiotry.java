package home.reposiotries;

import home.entities.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.math.BigInteger;

public interface BookReposiotry extends JpaRepository<Book, BigInteger> {
}
