package home.reposiotries;

import home.entities.cassandra.Book2;
import org.springframework.data.repository.CrudRepository;

public interface Book2Repository extends CrudRepository<Book2, String> {
}
